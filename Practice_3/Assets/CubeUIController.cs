﻿using UnityEngine;
using System.Collections;

public class CubeUIController : MonoBehaviour
{
	//declare GameObject named m_cube
	public GameObject m_cube;
	private TangoPointCloud m_pointCloud;

	//creates a reference to the TangoPointCloud script on the TangoPointCloud gameObject
	//this is used to call the FindPlane() method
	void Start()
	{
		m_pointCloud = FindObjectOfType<TangoPointCloud>();
	}

	//check the touch count and its state, is a touch has ended then place the cube
	void Update ()
	{
		if (Input.touchCount == 1)
		{
			// Trigger place cube function when single touch ended.
			Touch t = Input.GetTouch(0);
			if (t.phase == TouchPhase.Ended)
			{
				PlaceCube(t.position);
			}
		}
	}

	//places the cube in 3D space
	void PlaceCube(Vector2 touchPosition)
	{
		// Find the plane.
		Camera cam = Camera.main;
		Vector3 planeCenter;
		Plane plane;
		if (!m_pointCloud.FindPlane(cam, touchPosition, out planeCenter, out plane))
		{
			Debug.Log("cannot find plane.");
			return;
		}

		// Place cube on the surface, and make it always face the camera.
		if (Vector3.Angle(plane.normal, Vector3.up) < 100.0f)
		{
			Vector3 up = plane.normal;
			Vector3 right = Vector3.Cross(plane.normal, cam.transform.forward).normalized;
			Vector3 forward = Vector3.Cross(right, plane.normal).normalized;
			Instantiate(m_cube, planeCenter, Quaternion.LookRotation(forward, up));
		}
		else
		{
			Debug.Log("surface is too steep for cube to be placed on.");
		}
	}
}